﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlickCharacterController : MonoBehaviour {

    public float forceScale;

    public float timeToRotate;

    public GameObject toaster;

    public GameObject rightStickPrompt;

    public GameObject mainCamera;

    private bool _moved;

    private bool _rotationBegun;

    private float _moveCameraToSpaceManTimer;

    private Vector3 _cameraStartPosition;

	// Use this for initialization
	void Start () {
        _moved = false;
        _rotationBegun = false;
        _moveCameraToSpaceManTimer = 0;

        _cameraStartPosition = mainCamera.transform.position;
    }
	
	// Update is called once per frame
	void Update () {

        if (_moveCameraToSpaceManTimer < 2.0f)
        {
            GameObject cameraToMove = mainCamera;

            cameraToMove.transform.position -= new Vector3((_cameraStartPosition.x / 2.0f) * Time.deltaTime, (_cameraStartPosition.y / 2.0f) * Time.deltaTime, 0);
            _moveCameraToSpaceManTimer += Time.deltaTime;
        }
        else if (_moved == false)
        {
            GameObject cameraToMove = mainCamera;

            cameraToMove.transform.position = new Vector3(0, 0, cameraToMove.transform.position.z);

            rightStickPrompt.SetActive(true);

            // Flick character
            FlickCharacter();

            mainCamera.transform.position = new Vector3(transform.position.x, transform.position.y, mainCamera.transform.position.z);
        }
        else if (_rotationBegun == false)
        {
            mainCamera.transform.position = new Vector3(transform.position.x, transform.position.y, mainCamera.transform.position.z);

            RotateCamera();
        }
        else
        {
            mainCamera.transform.position = new Vector3(transform.position.x, transform.position.y, mainCamera.transform.position.z);
        }

	}

    // Read inputs and flick character
    void FlickCharacter()
    {
        float x = Input.GetAxis("RightHorizontal");
        float y = Input.GetAxis("RightVertical");
        Vector2 flickDirection = new Vector2(x, y);

        if (flickDirection.magnitude > 1)
        {
            flickDirection.Normalize();

            Rigidbody rb = GetComponent<Rigidbody>();
            rb.AddForce(flickDirection * forceScale, ForceMode.Impulse);

            rb.angularDrag = 0;
            rb.angularVelocity = Vector3.forward * 3.14f;

            _moved = true;

            mainCamera.GetComponent<CameraShake>().ShakeMovingCamera(transform);
        }
        
    }

    // Rotate camera to follow direction of spaceboy
    void RotateCamera()
    {
        Vector2 characterDirection = new Vector2(GetComponent<Rigidbody>().velocity.x, GetComponent<Rigidbody>().velocity.y).normalized;

        Vector2 up = Vector2.up;

        float amountToRotate = Vector2.Angle(up, characterDirection);

        if (characterDirection.x > 0)
        {
            amountToRotate *= -1;
        }

        _rotationBegun = true;

        rightStickPrompt.SetActive(false);

        SpawnToaster(characterDirection.normalized);

        StartCoroutine(RotateCameraCoroutine(amountToRotate));
    }

    // Rotate camera
    IEnumerator RotateCameraCoroutine(float angleToRotateTo)
    {
        GameObject cameraToRotate = mainCamera;

        float timer = 0;

        while (timer < timeToRotate)
        {
            cameraToRotate.transform.eulerAngles = cameraToRotate.transform.eulerAngles + new Vector3(0, 0, (angleToRotateTo / timeToRotate) * Time.deltaTime);

            timer += Time.deltaTime;

            yield return null;
        }


        cameraToRotate.transform.rotation = Quaternion.Euler(0, 0, angleToRotateTo);

        yield return null;
    }

    // Spawn toaster
    void SpawnToaster(Vector2 characterDirection)
    {
        characterDirection.Normalize();

        // Find position
        Vector3 positionToSpawn = transform.position + new Vector3(characterDirection.x * 3000.0f, characterDirection.y * 3000.0f, 0);
        
        // Find rotation
        Vector2 up = Vector2.up;

        float amountToRotate = Vector2.Angle(up, characterDirection * -1);
        
        if (characterDirection.x < 0)
        {
            amountToRotate *= -1;
        }

        StartCoroutine(WaitThenTurnOffLight());

        Instantiate(toaster, positionToSpawn, Quaternion.Euler(0, 0, amountToRotate));
    }

    // When it reaches the toaster
    void OnTriggerEnter(Collider other)
    {
        GameObject otherObject = other.gameObject;

        if (otherObject.tag == "Toaster")
        {
            //GameObject camera = transform.GetChild(0).gameObject;
            //camera.transform.parent = otherObject.transform;
            
            SceneManager.LoadScene("ToasterScene");
        }
    }

    IEnumerator WaitThenTurnOffLight()
    {
        yield return new WaitForSeconds(3.5f);
        
        GameObject.Find("Directional Light").GetComponent<Light>().intensity = 0;
    }
}