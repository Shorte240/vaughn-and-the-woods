﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalTracker : MonoBehaviour {

    public AudioClip[] clips;

    private AudioSource audio;

    private float timer;

	// Use this for initialization
	void Awake () {
        DontDestroyOnLoad(gameObject);

        timer = 0;

        audio = GetComponent<AudioSource>();
        audio.clip = clips[Random.Range(0, clips.Length)];
        audio.Play();
	}
	
    public float GetTime()
    {
        return timer;
    }

	// Update is called once per frame
	void Update () {

        if (SceneManager.GetActiveScene().name != "ScoreScene")
        {
            timer += Time.deltaTime;
        }
        else
        {
            Debug.Log(timer);
        }


        if (audio.isPlaying == false)
        {
            audio.clip = clips[Random.Range(0, clips.Length)];
            audio.Play();
        }

	}
}
