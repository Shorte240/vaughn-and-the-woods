﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HouseMoveController : MonoBehaviour {

    public float houseMoveSpeed;

    public GameObject particleObject;

    private bool _fellThrough;

	// Use this for initialization
	void Start () {
        _fellThrough = false;
        particleObject.SetActive(false);
        GetComponent<Rigidbody>().useGravity = false;
    }
	
	// Update is called once per frame
	void Update () {

        MoveHouse();

        if (Mathf.Abs(GetComponent<Rigidbody>().velocity.magnitude) > 5.0f && particleObject.activeSelf == false)
        {
            particleObject.SetActive(true);
        }
        else if (Mathf.Abs(GetComponent<Rigidbody>().velocity.magnitude) < 5.0f && particleObject.activeSelf == true)
        {
            particleObject.SetActive(false);
        }

	}

    // Collect coins
    void OnTriggerEnter(Collider other)
    {

    }

    // Move house with joystick input
    void MoveHouse()
    {

        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            Rigidbody rb = GetComponent<Rigidbody>();

            Vector3 force = new Vector3(houseMoveSpeed * Input.GetAxis("Horizontal"), 0, houseMoveSpeed * Input.GetAxis("Vertical"));

            rb.AddForceAtPosition(force, transform.position + (Vector3.up * 5.0f));

            if (GetComponent<Rigidbody>().useGravity == false)
            {
                GetComponent<Rigidbody>().useGravity = true;
            }
        }
    }

    public void DestroyCollider()
    {
        Destroy(GetComponent<Collider>());
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().AddForce(Vector3.down * 600, ForceMode.Impulse);
        _fellThrough = true;

        Destroy(GameObject.Find("Canvas"));

        StartCoroutine(FellThrough(GameObject.Find("Post Processing Camera")));

    }

    IEnumerator FellThrough(GameObject cameraToMove)
    {
        float timer = 0;

        Vector3 beginningRotation = cameraToMove.transform.eulerAngles;

        cameraToMove.transform.rotation = Quaternion.Euler(Vector3.zero);

        while (timer < 3.0f)
        {
            cameraToMove.transform.position = new Vector3(cameraToMove.transform.position.x, transform.position.y, cameraToMove.transform.position.z);

            timer += Time.deltaTime;

            GetComponent<Rigidbody>().AddForce(Vector3.down * 200, ForceMode.Impulse);

            yield return null;
        }

        yield return new WaitForSeconds(1.0f);

        SceneManager.LoadScene("ScoreScene");

    }
}
