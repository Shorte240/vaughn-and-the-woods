﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    // Variables to control the player
    public float movementSpeed;
    public float jumpHeight;
    int jumpCounter;
    public int maxJumpCounter;
    public GameObject buttonPrompt;
    public GameObject imposter;
    public Vector3 playCameraPosition;
    public float cameraSpeed;

    private GameObject mainCamera;
    private bool _cameraMoved;

    // Use this for initialization
    void Start ()
    {
        buttonPrompt.SetActive(false);
        GetComponent<Rigidbody>().velocity = new Vector3(0.0f, 0.0f, 0.0f);

        mainCamera = GameObject.Find("Post Processing Camera");
        _cameraMoved = false;

        GetComponent<Animator>().speed = 0;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Mathf.Abs((playCameraPosition - mainCamera.transform.position).magnitude) > 2.0f && _cameraMoved == false)
        {

            Vector3 moveDirection = playCameraPosition - mainCamera.transform.position;
            moveDirection.Normalize();

            mainCamera.transform.position += moveDirection * Time.deltaTime * cameraSpeed;
        }
        else
        {
            if (_cameraMoved == false)
            {
                _cameraMoved = true;
            }

            // Move the player right
            MovePlayer();

            // Make the player jump
            Jump();

            Transform cameraTransform = GameObject.Find("Post Processing Camera").transform;
            cameraTransform.position = new Vector3(transform.position.x, cameraTransform.position.y, cameraTransform.position.z);

            if (transform.position.x > -22.0f && buttonPrompt.activeSelf == false)
            {
                buttonPrompt.SetActive(true);
            }

            if (transform.position.y > 50)
            {
                SceneManager.LoadScene("SpacemanScene");
            }

            if (transform.position.y > 1.1f)
            {
                GetComponent<Animator>().speed = 0;
            }
            else
            {
                GetComponent<Animator>().speed = 1;
            }
        }
	}

    // Moves the player right
    void MovePlayer()
    {
        transform.position += new Vector3(movementSpeed, 0.0f, 0.0f) * Time.deltaTime;
        imposter.transform.position = new Vector3(transform.position.x, 0.51f, imposter.transform.position.z);
    }

    // Makes the player jump
    void Jump()
    {
        if (Input.GetAxis("Jump") > 0)
        {
            if (transform.position.y < 1.1f && transform.position.x > -22.0f && jumpCounter != maxJumpCounter) //GetComponent<Rigidbody>().velocity.y == 0
            {
                //transform.position += new Vector3(0.0f, jumpHeight, 0.0f) * Time.deltaTime;
                GetComponent<Rigidbody>().velocity += new Vector3(0.0f, jumpHeight, 0.0f) * Time.deltaTime;
                jumpCounter += 1;
            }
        }
        if (jumpCounter >= maxJumpCounter)
        {
            GetComponent<Rigidbody>().useGravity = false;
            GetComponent<Rigidbody>().velocity = new Vector3(0.0f, 600.0f, 0.0f) * Time.deltaTime;
            
        }
    }

    // Check for collision against hurdles
    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Hurdle")
        {
            // Reset the scene, the player lost
            FindObjectOfType<AudioSource>().Stop();
            Destroy(FindObjectOfType<GlobalTracker>());
            SceneManager.LoadScene("RunningScene");
        }
        else if (collision.collider.tag == "Jump Trigger")
        {
            // Cause the player to go up endlessly
            //transform.position += new Vector3(0.0f, jumpHeight, 0.0f) * Time.deltaTime;
            GetComponent<Rigidbody>().useGravity = false;
            GetComponent<Rigidbody>().velocity += new Vector3(0.0f, jumpHeight, 0.0f) * Time.deltaTime;
        }
    }
}
