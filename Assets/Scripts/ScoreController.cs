﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : MonoBehaviour {
    
    public TextMesh scoreText;

    public bool collectathon;

    private int _score;

    // Use this for initialization
    void Start()
    {
        _score = 0;
        scoreText.text = _score.ToString();

        if (collectathon == true)
        {
            scoreText.text = _score.ToString() + "/5";
        }
    }
	
	// Update is called once per frame
	void Update () {

    }

    public bool AddScore()
    {
        _score += 1;

        scoreText.text = _score.ToString();

        if (collectathon == true)
        {
            scoreText.text = _score.ToString() + "/5";

            if (_score == 5)
            {
                return true;
            }
        }

        return false;
    }
}
