﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreSceneController : MonoBehaviour {

    public GameObject scoreScreen;
    public GameObject presentsScreen;

    public TextMesh scoreText;

    private bool _scoreShown;

	// Use this for initialization
	void Start () {

        StartCoroutine(FadeInScoreScreen());
        _scoreShown = false;

        scoreText.text = GameObject.FindObjectOfType<GlobalTracker>().GetTime().ToString("F2") + " SECS";

    }
	
	// Update is called once per frame
	void Update () {
		
        if (_scoreShown == true && Input.GetKeyDown("joystick button 1"))
        {
            StartCoroutine(FadeOutScoreScreen());
        }

	}

    IEnumerator FadeInScoreScreen()
    {

        while (scoreScreen.transform.localScale.x < 1)
        {
            scoreScreen.transform.localScale += (new Vector3(1, 1, 1)) * Time.deltaTime;

            yield return null;

        }

        scoreScreen.transform.localScale = new Vector3(1, 1, 1);

        _scoreShown = true;
    }

    IEnumerator FadeOutScoreScreen()
    {

        while (scoreScreen.transform.localScale.x > 0)
        {
            scoreScreen.transform.localScale -= new Vector3(1, 1, 1) * Time.deltaTime;

            yield return null;
        }

        StartCoroutine(FadeInAndOutPresents());
    }

    IEnumerator FadeInAndOutPresents()
    {
        while (presentsScreen.transform.localScale.x < 10)
        {
            presentsScreen.transform.localScale += new Vector3(10, 5, 5) * Time.deltaTime;

            yield return null;

        }

        presentsScreen.transform.localScale = new Vector3(10, 5, 5);

        yield return new WaitForSeconds(2.5f);

        while (presentsScreen.transform.localScale.x > 0)
        {
            presentsScreen.transform.localScale -= new Vector3(10, 5, 5) * Time.deltaTime;

            yield return null;
        }

        Application.Quit();
    }
}
