﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToastScript : MonoBehaviour {

    // Cameras
    public Camera frontCamera;
    public Camera sideCamera;

    // Value to alter the hanldes height
    public float yOffset;

    // Timer variables
    bool timerRotating;
    public float timerRotationSpeed;
    public GameObject PPSquare;
    public GameObject squareObj;
    public GameObject triangleObj;
    public GameObject leftTriggerObj;
    public GameObject rightTriggerObj;

    // Toast vars
    bool toastFired;
    bool toastIsIn;
    bool toasted;

    public float colorTransitionSpeed;

    // Use this for initialization
    void Start() {
        timerRotating = false;
        sideCamera.enabled = false;
        frontCamera.enabled = true;
        toastFired = false;
        toastIsIn = false;
        toasted = false;
        squareObj.SetActive(true);
        triangleObj.SetActive(false);
        leftTriggerObj.SetActive(false);
        rightTriggerObj.SetActive(false);

        StartCoroutine(WaitThenTurnOnLight());
    }

    // Update is called once per frame
    void Update() {
        // BEGIN THE TOASTING
        CheckForButton();

        if (transform.localPosition.y <= 0.2f && !timerRotating)
        {
            RotateTimer();
        }

        if (toastFired)
        {
            GameObject.FindGameObjectWithTag("Toast").GetComponent<Rigidbody>().useGravity = true;
            GameObject.Find("Post Processing Camera").transform.position = new Vector3(GameObject.Find("Post Processing Camera").transform.position.x, GameObject.FindGameObjectWithTag("Toast").transform.position.y, GameObject.Find("Post Processing Camera").transform.position.z);

            // 0 , 12 , 255 Color.Lerp
            //frontCamera.backgroundColor = Color.Lerp(startingColor, new Color(0.0f, 0.0f, 191.0f, 255.0f), Mathf.PingPong(Time.deltaTime, 1));
            frontCamera.backgroundColor += new Color(0.0f,0.0f, Time.deltaTime/colorTransitionSpeed);
            StartCoroutine(WaitThenTransition());
        }
    }

    // Check for a button input to toast
    void CheckForButton()
    {
        if (transform.localPosition.y < 0.2f && squareObj.activeSelf == true)
        {
            squareObj.SetActive(false);
            triangleObj.SetActive(true);
        }
        if (!toastIsIn)
        {
            if (Input.GetAxis("Square") > 0 || Input.GetKey("space"))
            {
                frontCamera.enabled = false;
                sideCamera.enabled = true;

                if (transform.localPosition.y >= 0.2f)
                {
                    transform.localPosition -= new Vector3(0.0f, yOffset, 0.0f);
                    
                }
                else if (transform.localPosition.y <= 0.2f)
                {
                    toastIsIn = true;
                    sideCamera.GetComponent<CameraShake>().ShakeSetCamera();
                }
            } 
        }
        if (toastIsIn)
        {
            if (!toasted)
            {

                if (Input.GetAxis("Triangle") > 0 || Input.GetKey("v"))
                {
                    GameObject.FindGameObjectWithTag("Timer").transform.rotation = GameObject.FindGameObjectWithTag("Timer").transform.rotation;
                    timerRotating = true;
                    triangleObj.SetActive(false);
                    leftTriggerObj.SetActive(true);
                    rightTriggerObj.SetActive(true);
                    toasted = true;

                    sideCamera.GetComponent<CameraShake>().StopShaking();
                } 
            }
            if (toasted)
            {
                if (Input.GetAxis("LeftTrigger") == 1 && Input.GetAxis("RightTrigger") == 1 || Input.GetKey("c") && Input.GetKey("b"))
                {
                    // Eject the toast
                    toastFired = true;
                    sideCamera.enabled = false;
                    frontCamera.enabled = true;
                    squareObj.SetActive(false);
                    leftTriggerObj.SetActive(false);
                    rightTriggerObj.SetActive(false);
                    triangleObj.SetActive(false);
                    PPSquare.SetActive(false);
                }  
            }
        }

        if (Input.GetKeyDown("s"))
        {
            GameObject.FindGameObjectWithTag("Timer").transform.rotation = GameObject.FindGameObjectWithTag("Timer").transform.rotation;
            timerRotating = true;
            squareObj.SetActive(false);
            triangleObj.SetActive(false);
            leftTriggerObj.SetActive(true);
            rightTriggerObj.SetActive(true);
            toastIsIn = true;
            toasted = true;

            sideCamera.GetComponent<CameraShake>().StopShaking();
        }
    }

    // Rotate the timer
    void RotateTimer()
    {
        GameObject.FindGameObjectWithTag("Timer").transform.Rotate(new Vector3(timerRotationSpeed, 0.0f, 0.0f));
    }
    IEnumerator WaitThenTurnOnLight()
    {
        yield return new WaitForSeconds(0.5f);
        GameObject.Find("Directional Light").GetComponent<Light>().intensity = 1;
    }

    IEnumerator WaitThenTransition()
    { 
        yield return new WaitForSeconds(10.0f);
        SceneManager.LoadScene("ComputerHouseScene");
    }
}
