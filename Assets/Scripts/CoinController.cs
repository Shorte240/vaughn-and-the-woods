﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour {

    public float rotationSpeed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + (rotationSpeed * Time.deltaTime), -90);

	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            bool finished = other.gameObject.GetComponent<ScoreController>().AddScore();

            if (finished == true)
            {
                other.gameObject.GetComponent<HouseMoveController>().DestroyCollider();
            }

            Destroy(gameObject);
        }
    }
}
