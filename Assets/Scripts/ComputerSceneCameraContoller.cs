﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ComputerSceneCameraContoller : MonoBehaviour {

    public Vector3 cameraEndpoint;
    public Vector3 cameraRotationEndpoint;

    public float cameraMoveSpeed;
    public float cameraRotationSpeed;

    public GameObject textToDisable;

    private bool _moveCamera;

    private bool _moveFinished;
    private bool _rotationFinished;

	// Use this for initialization
	void Start () {
        _moveCamera = false;
        _moveFinished = false;
        _rotationFinished = false;

    }
	
	// Update is called once per frame
	void Update () {

        if (_moveCamera == true)
        {
            if (Mathf.Abs((cameraEndpoint - transform.position).magnitude) > 5.0f)
            {
                transform.position = Vector3.Lerp(transform.position, cameraEndpoint, Time.deltaTime * cameraMoveSpeed);
            }
            else if (transform.position != cameraEndpoint)
            {
                transform.position = cameraEndpoint;
                _moveFinished = true;
            }

            if (Mathf.Abs((cameraRotationEndpoint - transform.eulerAngles).magnitude) > 10.0f)
            {
                transform.rotation = Quaternion.Euler(Mathf.Lerp(transform.eulerAngles.x, cameraRotationEndpoint.x, Time.deltaTime * cameraRotationSpeed), 0, 0);
            }
            else if (transform.eulerAngles != cameraRotationEndpoint)
            {
                transform.rotation = Quaternion.Euler(cameraRotationEndpoint);
                _rotationFinished = true;
            }
            
            if (_moveFinished == true && _rotationFinished == true)
            {
                StartCoroutine(WaitAndTransition());
            }
        }

	}

    IEnumerator WaitAndTransition()
    {
        yield return new WaitForSeconds(1.0f);

        SceneManager.LoadScene("HouseScene");
    }

    public void StartCameraMove()
    {
        _moveCamera = true;
        Destroy(textToDisable);
    }
}
