﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerTypingController : MonoBehaviour {

    public string textToType;

    public int charactersPerRow;

    public TextMesh textBox;

    public ComputerSceneCameraContoller cameraController;

    private string _charactersTyped;

    private int _characterCount;

	// Use this for initialization
	void Start () {

        _charactersTyped = "";
        _characterCount = 0;
    }
	
	// Update is called once per frame
	void Update () {

        if (_characterCount < textToType.Length)
        {
            if (Input.GetKeyDown("joystick button 0") || Input.GetKeyDown("joystick button 1") || Input.GetKeyDown("joystick button 2") || Input.GetKeyDown("joystick button 3") || Input.GetAxis("PadX") != 0 || Input.GetAxis("PadY") != 0)
            {
                _charactersTyped += textToType[_characterCount];

                textBox.text = _charactersTyped;

                _characterCount += 1;

                if (_characterCount % charactersPerRow == 0)
                {
                    _charactersTyped += "\n";
                }

                cameraController.gameObject.GetComponent<CameraShake>().ShakeSetCamera();
            }
        }
        else
        {
            StartCoroutine(WaitBeforeCameraMove());
        }

	}

    IEnumerator WaitBeforeCameraMove()
    {
        yield return new WaitForSeconds(2.0f);

        cameraController.StartCameraMove();
    }
}
